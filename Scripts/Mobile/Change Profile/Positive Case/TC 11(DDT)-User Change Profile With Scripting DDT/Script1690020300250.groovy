import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

TestData testData = findTestData('Data Files/Change Profile Data/changeProfileMobile')

WebUI.callTestCase(findTestCase('Mobile/Change Profile/Positive Case/TC 10 - User membuka halaman Profile'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Mobile/Profile Page/settingButton'), 0)

Mobile.tap(findTestObject('Mobile/Profile Page/editProfile'), 0)

Mobile.tap(findTestObject('Mobile/Profile Page/editFullname'), 0)

for (int rowData = 1; rowData <= testData.getRowNumbers(); rowData++) {
    String fullname = testData.getValue(1, rowData)

    String phone = testData.getValue(2, rowData)

    Mobile.sendKeys(findTestObject('Mobile/Profile Page/editFullname'), fullname, FailureHandling.STOP_ON_FAILURE)

    Mobile.tap(findTestObject('Mobile/Profile Page/editPhone'), 0)

    Mobile.sendKeys(findTestObject('Mobile/Profile Page/editPhone'), phone, FailureHandling.STOP_ON_FAILURE)

    Mobile.hideKeyboard()

    if (fullname == '!#!@$@') {
        Mobile.verifyElementText(findTestObject('Mobile/Profile Page/verifyErrorName'), 'Cannot be empty and must be alphabetically')
		
		Mobile.clearText(findTestObject('Mobile/Profile Page/editFullname'), 0)
		
		Mobile.clearText(findTestObject('Mobile/Profile Page/editPhone'), 0)
    } else if (fullname == 'Aditya Tri') {
        Mobile.verifyElementText(findTestObject('Mobile/Profile Page/verifyNonNumericNumber'), 'WhatsApp must be numeric')
		
		Mobile.clearText(findTestObject('Mobile/Profile Page/editFullname'), 0)
		
		Mobile.clearText(findTestObject('Mobile/Profile Page/editPhone'), 0)
    } else if (phone == '085213402300') {
        Mobile.verifyElementText(findTestObject('Mobile/Profile Page/verifyErrorName'), 'Cannot be empty and must be alphabetically')
		
		Mobile.clearText(findTestObject('Mobile/Profile Page/editFullname'), 0)
		
		Mobile.clearText(findTestObject('Mobile/Profile Page/editPhone'), 0)
		
    } else if (fullname == 'Kurniawan') {
        Mobile.verifyElementText(findTestObject('Mobile/Profile Page/verifyEmptyNumber'), 'WhatsApp number is required')
		
		Mobile.clearText(findTestObject('Mobile/Profile Page/editFullname'), 0)
		
		Mobile.clearText(findTestObject('Mobile/Profile Page/editPhone'), 0)
    } else if (fullname == 'Muhammad Hafidz') {
        Mobile.tap(findTestObject('Mobile/Profile Page/buttonSaveChanges'), 0)

        Mobile.tap(findTestObject('Mobile/Profile Page/buttonOkay'), 0)

        Mobile.verifyElementText(findTestObject('Mobile/Profile Page/verifyAfterChangeProfile'), fullname)
    } else {
        Mobile.verifyElementText(findTestObject('Mobile/Profile Page/verify9-13Number'), 'Number consists of 9 - 13 characters')
		
		Mobile.clearText(findTestObject('Mobile/Profile Page/editFullname'), 0)
		
		Mobile.clearText(findTestObject('Mobile/Profile Page/editPhone'), 0)
    }
}



