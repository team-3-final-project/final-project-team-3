import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Webiste/Register/Positive Case/TC 81 - User membuka halaman Register'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Website/Register/inputUsername'), 'Hasby hasby')

WebUI.setText(findTestObject('Website/Register/inputTanggalLahir'), '11-Jul-1990')

WebUI.setText(findTestObject('Website/Register/inputEmail'), 'hasbyramadhan667@gmail.com')

WebUI.setText(findTestObject('Website/Register/inputWhatsapp'), '082e')

WebUI.setText(findTestObject('Website/Register/inputKataSandi'), '123123123')

WebUI.setText(findTestObject('Website/Register/inputKonfirmasiKataSandi'), '123123123')

WebUI.click(findTestObject('Website/Register/InputCheckBox'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Website/Register/buttonDaftar'))

WebUI.verifyEqual(WebUI.getAttribute(findTestObject('Website/Register/inputWhatsapp'), 'validationMessage'), 'Please enter a number.')

