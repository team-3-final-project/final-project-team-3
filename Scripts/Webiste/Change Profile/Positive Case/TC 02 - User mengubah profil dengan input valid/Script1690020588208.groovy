import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Webiste/Change Profile/Positive Case/TC 01 - User membuka halaman Profile'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Website/Change Profile Page/editProfile'))

WebUI.uploadFile(findTestObject('Website/Change Profile Page/uploadFile'), 'E:\\BOOTCAMP\\Advance\\Meet 7\\anjing.jpeg')

WebUI.setText(findTestObject('Website/Change Profile Page/inputFullName'), 'Aditya Tri')

WebUI.setText(findTestObject('Website/Change Profile Page/inputPhone'), '081292123112')

WebUI.setText(findTestObject('Website/Change Profile Page/inputBirthDate'), '01-Mar-2000')

WebUI.click(findTestObject('Website/Change Profile Page/button_Save Changes'))

WebUI.verifyElementText(findTestObject('Website/Change Profile Page/verifyNameAfterChange'), 'Aditya Tri')

