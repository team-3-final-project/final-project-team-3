<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>buttonContinuePayment</name>
   <tag></tag>
   <elementGuidId>3f6db45e-c7e4-4c14-99da-de242c4ad5eb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#continue_pay</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='continue_pay']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>d47228f3-7ae3-4fe3-a3f6-811110d19587</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>#</value>
      <webElementGuid>2067323f-bf73-42fd-97a8-b62f150511a6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-primary</value>
      <webElementGuid>ed5603f8-a171-4e4c-9c9f-f2dfd8efad6f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>continue_pay</value>
      <webElementGuid>3cbf7091-5fd1-4ad5-a108-e110b0bc2cf9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Continue Payment</value>
      <webElementGuid>2ac67215-f4a5-4da3-8dd9-51969cf3ebdf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;continue_pay&quot;)</value>
      <webElementGuid>bd99e325-8230-483f-a105-08712b19f519</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//a[@id='continue_pay']</value>
      <webElementGuid>680032cf-2c92-4fde-bd9b-b6ea0ca66cb7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div[3]/section/div/div/div/div/div[3]/div[3]/div/ul/a</value>
      <webElementGuid>1122b25e-abbb-4b97-b134-0229b3205291</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Continue Payment')]</value>
      <webElementGuid>cb6e3287-b190-40dc-b98e-e80046aa20a3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Complete your payment here :'])[1]/following::a[1]</value>
      <webElementGuid>37ed2bff-e123-4a3c-8ad7-25e32898564e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Invoice Date :'])[1]/following::a[1]</value>
      <webElementGuid>2a85bdde-86d3-4c31-8a6e-d142eaea62d2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Order Summary'])[1]/preceding::a[2]</value>
      <webElementGuid>7565d27f-dbc2-480b-93e6-5ea4189f6124</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Continue Payment']/parent::*</value>
      <webElementGuid>e18c902e-9ded-4fec-a707-5a6d0b60b265</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[contains(@href, '#')])[4]</value>
      <webElementGuid>e27eec6c-660a-4269-a581-520683cd7bed</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//ul/a</value>
      <webElementGuid>c964b4ed-e054-409b-bcf0-9fa7bbf1ec1b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '#' and @id = 'continue_pay' and (text() = 'Continue Payment' or . = 'Continue Payment')]</value>
      <webElementGuid>b0053b28-dad3-4307-9200-a7a7f93af31c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
