<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>day4Workshop</name>
   <tag></tag>
   <elementGuidId>7a98f52e-b6e5-42ed-93ff-78654639ab7b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//li[@id='blockListEvent']/a/div)[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>9c0eac83-8cf3-49f3-8d1c-a91e32a376c0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>cardOuter</value>
      <webElementGuid>40b0ce1c-8680-4e47-b0f2-c2b823e5d682</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                        
                                            
                                                
                                                                                                    
                                                    
                                                    
                                                                                            

                                            
                                                
                                                    Day 4: Workshop
                                                    
                                                
                                                
                                                    With Ziyad Syauqi Fawwazi
                                                    
                                                
                                            

                                        
                                        
                                                                                            OPEN until
                                                        25 Nov 2023 11:30 WIB
                                                
                                                                                        Mini Class
                                            
                                            
                                                Day 4: Workshop
                                                
                                            
                                            
                                                25 Nov 2023 |
                                                19:30 WIB Via
                                                Zoom
                                                
                                            
                                                                                                                                                Rp.
                                                            500.000

                                                                                                                    Rp
                                                                85.000
                                                                                                            
                                                                                                                                    
                                    </value>
      <webElementGuid>0eed9f20-1d15-4f0f-873b-1f43b48b9d33</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;loopevent&quot;)/li[@id=&quot;blockListEvent&quot;]/a[1]/div[@class=&quot;cardOuter&quot;]</value>
      <webElementGuid>0ed8f668-1957-46bf-86ea-f744bcf507b5</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>(//li[@id='blockListEvent']/a/div)[2]</value>
      <webElementGuid>6893762e-0f20-4bf8-af8c-ca48ecc661d9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[2]/a/div</value>
      <webElementGuid>b1105e82-ec67-4b42-9dd4-306379934a17</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                                        
                                            
                                                
                                                                                                    
                                                    
                                                    
                                                                                            

                                            
                                                
                                                    Day 4: Workshop
                                                    
                                                
                                                
                                                    With Ziyad Syauqi Fawwazi
                                                    
                                                
                                            

                                        
                                        
                                                                                            OPEN until
                                                        25 Nov 2023 11:30 WIB
                                                
                                                                                        Mini Class
                                            
                                            
                                                Day 4: Workshop
                                                
                                            
                                            
                                                25 Nov 2023 |
                                                19:30 WIB Via
                                                Zoom
                                                
                                            
                                                                                                                                                Rp.
                                                            500.000

                                                                                                                    Rp
                                                                85.000
                                                                                                            
                                                                                                                                    
                                    ' or . = '
                                        
                                            
                                                
                                                                                                    
                                                    
                                                    
                                                                                            

                                            
                                                
                                                    Day 4: Workshop
                                                    
                                                
                                                
                                                    With Ziyad Syauqi Fawwazi
                                                    
                                                
                                            

                                        
                                        
                                                                                            OPEN until
                                                        25 Nov 2023 11:30 WIB
                                                
                                                                                        Mini Class
                                            
                                            
                                                Day 4: Workshop
                                                
                                            
                                            
                                                25 Nov 2023 |
                                                19:30 WIB Via
                                                Zoom
                                                
                                            
                                                                                                                                                Rp.
                                                            500.000

                                                                                                                    Rp
                                                                85.000
                                                                                                            
                                                                                                                                    
                                    ')]</value>
      <webElementGuid>ba66421f-3d1c-4775-bffd-02c69a3afa57</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
