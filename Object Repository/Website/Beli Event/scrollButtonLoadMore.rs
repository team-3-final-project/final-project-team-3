<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>scrollButtonLoadMore</name>
   <tag></tag>
   <elementGuidId>0e30b3ac-59f3-4390-abf3-35ad0c459f17</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//li[@id='blockListEvent']/a/div/div)[15]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>08921f44-9b55-4b6c-9b29-0cbc78e54923</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>blockHeader</value>
      <webElementGuid>b7e21a5d-b80c-4fc4-a4d0-3bcdbd209be6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                            
                                                
                                                                                                    
                                                    
                                                    
                                                                                            

                                            
                                                
                                                    Shifting Career To Data Science
                                                    
                                                
                                                
                                                    With Eric Julianto
                                                    
                                                
                                            

                                        </value>
      <webElementGuid>09f44332-7393-456c-8b93-b7868c5aa464</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;loopevent&quot;)/li[@id=&quot;blockListEvent&quot;]/a[1]/div[@class=&quot;cardOuter&quot;]/div[@class=&quot;blockHeader&quot;]</value>
      <webElementGuid>b916bf7b-588a-4e8a-b8d1-451f46835e64</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>(//li[@id='blockListEvent']/a/div/div)[15]</value>
      <webElementGuid>3c5a7736-5b78-4429-b207-dc829b286487</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='FREE'])[2]/following::div[2]</value>
      <webElementGuid>c63d73b8-c042-4649-afac-d0bd0d53316e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[8]/a/div/div</value>
      <webElementGuid>545a25c6-0bd8-4369-b076-5c2352c57dd2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                                            
                                                
                                                                                                    
                                                    
                                                    
                                                                                            

                                            
                                                
                                                    Shifting Career To Data Science
                                                    
                                                
                                                
                                                    With Eric Julianto
                                                    
                                                
                                            

                                        ' or . = '
                                            
                                                
                                                                                                    
                                                    
                                                    
                                                                                            

                                            
                                                
                                                    Shifting Career To Data Science
                                                    
                                                
                                                
                                                    With Eric Julianto
                                                    
                                                
                                            

                                        ')]</value>
      <webElementGuid>58351467-0da1-4e64-9c54-147657ca3709</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
