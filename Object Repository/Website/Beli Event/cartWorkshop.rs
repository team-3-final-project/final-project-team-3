<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>cartWorkshop</name>
   <tag></tag>
   <elementGuidId>12d0a686-f59d-4dc9-9a0d-327b64cd64fe</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.col-md-7 > div > h3</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='cartForm']/div/div/div[2]/div/h3</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h3</value>
      <webElementGuid>0b078ee3-8a63-43ed-9dfa-a815dc297202</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                    Day 4: Workshop</value>
      <webElementGuid>5c27eb98-e10e-4be5-b0c3-aba867a68282</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;cartForm&quot;)/div[@class=&quot;col-md-8&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-7&quot;]/div[1]/h3[1]</value>
      <webElementGuid>a60ed65f-9689-454d-9a17-fa53120dad2c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='cartForm']/div/div/div[2]/div/h3</value>
      <webElementGuid>4b4f71d1-bc1d-4d78-94d4-f6dd7dfa8672</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pembelian Saya'])[1]/following::h3[1]</value>
      <webElementGuid>b5bc4532-953e-4bf1-8238-235a71bdde08</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Event'])[1]/preceding::h3[1]</value>
      <webElementGuid>d9879c49-accc-4d6a-a8e2-7c7c50b05c97</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Day 4: Workshop']/parent::*</value>
      <webElementGuid>2545a2a3-21ee-4b54-85c5-15f233590933</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/h3</value>
      <webElementGuid>165bcb22-15c5-4bbf-ac15-940cd3c6035e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h3[(text() = '
                                                    Day 4: Workshop' or . = '
                                                    Day 4: Workshop')]</value>
      <webElementGuid>8ab420a2-a475-48f5-9c70-68c723c2507a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
